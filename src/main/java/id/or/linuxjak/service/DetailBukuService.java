package id.or.linuxjak.service;

import java.util.List;

import id.or.linuxjak.domain.DetailBuku;
import id.or.linuxjak.service.common.ServiceHelper;

public interface DetailBukuService extends ServiceHelper<DetailBuku>{
	List<DetailBuku> getAll();
}
