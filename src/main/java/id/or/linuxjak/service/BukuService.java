package id.or.linuxjak.service;

import java.util.List;

import id.or.linuxjak.domain.Buku;
import id.or.linuxjak.service.common.ServiceHelper;

public interface BukuService extends ServiceHelper<Buku>{
	List<Buku> getAll();
	Buku findBukuById(Long id);
	void update(Buku buku);
}
