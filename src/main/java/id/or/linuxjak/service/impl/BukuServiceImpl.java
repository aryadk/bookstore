package id.or.linuxjak.service.impl;

import java.util.List;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import id.or.linuxjak.domain.Buku;
import id.or.linuxjak.service.BukuService;
import id.or.linuxjak.service.common.ServiceHelperImpl;
@Service("bukuService")
@Transactional
public class BukuServiceImpl extends ServiceHelperImpl<Buku> implements BukuService {
	public List<Buku> getAll() {
		// TODO Auto-generated method stub
		return super.getAll(Buku.class);
	}

	public void update(Buku buku) {
		// TODO Auto-generated method stub
		genericDao.update(buku);
	}

	public Buku findBukuById(Long id) {
		// TODO Auto-generated method stub
		return super.find(Buku.class, id);
	}

}
