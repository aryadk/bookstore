package id.or.linuxjak.service.common;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import id.or.linuxjak.dao.GenericDao;

public abstract class ServiceHelperImpl<T> implements ServiceHelper<T>{

	@Autowired
	protected GenericDao<T> genericDao;
	
	
	@Transactional(readOnly = true)
	public List<T> getAll(Class<T> clazz) {
		// TODO Auto-generated method stub
		return genericDao.getAll(clazz);
	}

	@Transactional
	public long countAll(Map<Object, String> param) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Transactional
	public void create(T t) {
		// TODO Auto-generated method stub
		
	}

	@Transactional
	public void delete(T t) {
		// TODO Auto-generated method stub
		
	}

	@Transactional
	public T find(Class<T> clazz, Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	public void update(T t) {
		// TODO Auto-generated method stub
		
	}

}
