package id.or.linuxjak.util;

import java.util.Date;

import id.or.linuxjak.controller.domain.BukuControllerDomain;
import id.or.linuxjak.domain.Buku;
import id.or.linuxjak.domain.DetailBuku;

public class DomainControllertoDomain {

	public  static Buku BukuControllertoBuku(BukuControllerDomain bcd){
		Buku buku = new Buku();
		DetailBuku detBuku = new DetailBuku();
		
		detBuku.setPenerbit(bcd.getPenerbit());
		detBuku.setHarga(bcd.getHarga());
		detBuku.setPengarang(bcd.getPengarang());
		detBuku.setTahunTerbit(bcd.getTahunTerbit());
		detBuku.setBuku(buku);
		
		buku.setJudulBuku(bcd.getJudul());
		buku.setDetailBuku(detBuku);
		
		return buku;
		
	}
	
}
