package id.or.linuxjak.util;

import java.util.ArrayList;
import java.util.List;

import id.or.linuxjak.controller.domain.BukuControllerDomain;
import id.or.linuxjak.domain.Buku;



public class DomainToControllerDomain {

	public static BukuControllerDomain domaintoController(Buku buku){
		BukuControllerDomain bcd = new BukuControllerDomain();
		bcd.setJudul(buku.getJudulBuku());
		bcd.setHarga(buku.getDetailBuku().getHarga());
		bcd.setPengarang(buku.getDetailBuku().getPengarang());
		bcd.setPenerbit(buku.getDetailBuku().getPenerbit());
		bcd.setTahunTerbit(buku.getDetailBuku().getTahunTerbit());
		return bcd;
	}
	
	public static List<BukuControllerDomain> domaintoControllerList(List<Buku> list){
		List <BukuControllerDomain> listBcd = new ArrayList<BukuControllerDomain>();
		for(Buku buku : list){
			listBcd.add(domaintoController(buku));
		}
		return listBcd;
		
	}
	
}
