package id.or.linuxjak.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name="detailBuku")
public class DetailBuku {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idDetailBuku", unique = true, nullable = false)
	@GenericGenerator(name="gen", strategy = "foreign", parameters = @Parameter(name="property", value="buku"))
	private Long idDetailBuku;
	
	@Column(name = "penerbit", unique = false, nullable = false)
	private String penerbit;
	
	@Column(name = "pengarang", unique = false, nullable = false)
	private String pengarang;
	
	@Column(name = "tahunTerbit", unique = false, nullable = false)
	private String tahunTerbit;
	
	@Column(name = "harga", unique = false, nullable = false)
	private int harga; 
	
	@OneToOne(fetch=FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private Buku buku;

	public Long getIdDetailBuku() {
		return idDetailBuku;
	}

	public void setIdDetailBuku(Long idDetailBuku) {
		this.idDetailBuku = idDetailBuku;
	}

	public String getPenerbit() {
		return penerbit;
	}

	public void setPenerbit(String penerbit) {
		this.penerbit = penerbit;
	}

	public String getPengarang() {
		return pengarang;
	}

	public void setPengarang(String pengarang) {
		this.pengarang = pengarang;
	}

	public String getTahunTerbit() {
		return tahunTerbit;
	}

	public void setTahunTerbit(String tahunTerbit) {
		this.tahunTerbit = tahunTerbit;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

	public Buku getBuku() {
		return buku;
	}

	public void setBuku(Buku buku) {
		this.buku = buku;
	}
	
	
}
