package id.or.linuxjak.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name = "buku")
public class Buku {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idBuku",unique = true,nullable = false)
	private Long idBuku;
	
	@OneToOne(fetch = FetchType.LAZY,mappedBy = "buku",cascade = CascadeType.ALL)
	private DetailBuku detailBuku;
	
	@Column( name = "judulBuku",unique = false,nullable = false)
	private String judulBuku;

	public Long getIdBuku() {
		return idBuku;
	}

	public void setIdBuku(Long idBuku) {
		this.idBuku = idBuku;
	}

	public String getJudulBuku() {
		return judulBuku;
	}

	public void setJudulBuku(String judulBuku) {
		this.judulBuku = judulBuku;
	}

	public DetailBuku getDetailBuku() {
		return detailBuku;
	}

	public void setDetailBuku(DetailBuku detailBuku) {
		this.detailBuku = detailBuku;
	}
	
	
	
	
}
