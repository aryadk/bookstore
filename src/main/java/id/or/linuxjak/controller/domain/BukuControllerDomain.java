package id.or.linuxjak.controller.domain;

public class BukuControllerDomain {
	private Long id;
	private String judul;
	private int harga;
	private String penerbit;
	private String pengarang;
	private String tahunTerbit;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getJudul() {
		return judul;
	}
	public void setJudul(String judul) {
		this.judul = judul;
	}
	public int getHarga() {
		return harga;
	}
	public void setHarga(int harga) {
		this.harga = harga;
	}
	public String getPenerbit() {
		return penerbit;
	}
	public void setPenerbit(String penerbit) {
		this.penerbit = penerbit;
	}
	public String getPengarang() {
		return pengarang;
	}
	public void setPengarang(String pengarang) {
		this.pengarang = pengarang;
	}
	public String getTahunTerbit() {
		return tahunTerbit;
	}
	public void setTahunTerbit(String tahunTerbit) {
		this.tahunTerbit = tahunTerbit;
	}
}
