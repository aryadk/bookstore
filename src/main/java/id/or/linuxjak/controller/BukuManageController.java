package id.or.linuxjak.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import id.or.linuxjak.controller.domain.BukuControllerDomain;
import id.or.linuxjak.domain.Buku;
import id.or.linuxjak.domain.DetailBuku;
import id.or.linuxjak.service.BukuService;
import id.or.linuxjak.util.DomainControllertoDomain;
import id.or.linuxjak.util.DomainToControllerDomain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/user")
public class BukuManageController {

	
	@Autowired
	BukuService bukuService;
	
	private String message="";
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		 dateFormat.setLenient(false); 
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		
	}
	
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public ModelAndView addUserPage(){
		ModelAndView modelAndView = new ModelAndView("useradd");
		BukuControllerDomain bcd = new BukuControllerDomain();
		/*bcd.setJob("1");*/
		modelAndView.addObject("bcd",bcd);
		return modelAndView;
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public ModelAndView addUserProsses(@ModelAttribute("bcd") BukuControllerDomain bcd,
			BindingResult result, SessionStatus status){
		ModelAndView modelAndViewUser = new ModelAndView("useradd");
		ModelAndView modelAndView = new ModelAndView("redirect:/user/list");
		
	//	BindingResult errors = getBindingResult(result, bcd);
		
		if(result.hasErrors()){
			
			return modelAndViewUser;
		}else{
			status.setComplete();
			bukuService.create(DomainControllertoDomain.BukuControllertoBuku(bcd));
			message = "Buku successfull add";
		
			return modelAndView;
		}
		
		
	}
	
	
	@RequestMapping(value="/edit/{id}",method=RequestMethod.GET)
	public ModelAndView editUserPage(@PathVariable Long id){
		ModelAndView modelAndView = new ModelAndView("useredit");
		BukuControllerDomain bcd = DomainToControllerDomain.domaintoController(bukuService.findBukuById(id));
		modelAndView.addObject("action","/buku/edit/proccess");				
		modelAndView.addObject("url","/buku");		
		modelAndView.addObject("bcd",bcd);
		return modelAndView;
	}
	
	/*
	 * @ModelAttribute untuk model form dan ("bcd") untuk validator BindingResult result
	 */
	@RequestMapping(value="/edit/proccess",method=RequestMethod.POST)
	public ModelAndView editUserprosses(@ModelAttribute("bcd") BukuControllerDomain bcd,BindingResult result, SessionStatus status){
		ModelAndView modelAndView = new ModelAndView("redirect:/user/list");
		ModelAndView modelAndViewError = new ModelAndView("useredit");
		System.out.println(bcd.getId()+" id");
		if(result.hasErrors()){
			modelAndViewError.addObject("action","/buku/edit/proccess");				
			modelAndViewError.addObject("url","/buku");		
			modelAndViewError.addObject("bcd",bcd);
			
		return modelAndViewError;
		}else{
		
		Buku buku1 = bukuService.findBukuById(bcd.getId());
		DetailBuku dbuku = buku1.getDetailBuku();
		dbuku.setPenerbit(bcd.getPenerbit());
		dbuku.setPengarang(bcd.getPengarang());
		dbuku.setTahunTerbit(bcd.getTahunTerbit());
		dbuku.setHarga(bcd.getHarga());
		
		buku1.setJudulBuku(bcd.getJudul());
		buku1.setDetailBuku(dbuku);
	
	
		bukuService.update(buku1);
		message = "Buku successfull updated";
		return modelAndView;
		}
		}
	
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.GET)
	public ModelAndView deleteUserPage(@PathVariable Long id){
		ModelAndView modelAndView = new ModelAndView("redirect:/user/list");
		bukuService.delete(bukuService.findBukuById(id));
		message = "User successfull delete";
		return modelAndView;
	}
	
	
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public ModelAndView listUserPage(){
		ModelAndView modelandView = new ModelAndView("userList");
		List<BukuControllerDomain> listUcd = DomainToControllerDomain.domaintoControllerList(bukuService.getAll());
		
		modelandView.addObject("userList", listUcd);
		modelandView.addObject("message", message);
		
		message = "";
		return modelandView;
		
		
	}


	
	
	@ModelAttribute("jobList")
	public Map<String,String> getJobList(){
		Map<String,String> job = new LinkedHashMap<String,String>();
		job.put("1", "Pelajar");
		job.put("2", "Karyawan");
		job.put("3", "Wirausahawan");
		
		return job;
	}
	
	
	/*
	@RequestMapping(method=RequestMethod.POST)
	public String processSubmit(@ModelAttribute("bcd") BukuControllerDomain bcd,
			BindingResult result, SessionStatus status){
		
		userValidator.validate(bcd, result);
		
		if(result.hasErrors()){
			
			return "userView";
		}else{
			status.setComplete();
			bukuService.create(DomainControllertoDomain.UserControllertoUser(bcd));
			
			return "userSuccess";
		}
		
		
	}
	
	
	@RequestMapping(method=RequestMethod.GET)
	public String initForm(ModelMap model) {
		// TODO Auto-generated method stub
		BukuControllerDomain bcd = new BukuControllerDomain();
		bcd.setJob("1");
		model.addAttribute("bcd",bcd);
		return "userView";
		
	}
	

	
	*/
	
	
	
	
}
